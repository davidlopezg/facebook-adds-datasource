/* eslint-disable no-use-before-define */
let gulp = require('gulp');
let ts = require('gulp-typescript');
let tsProject = ts.createProject('tsconfig.json');

gulp.task('build', function () {
  return tsProject.src()
    .pipe(tsProject())
    .js.pipe(gulp.dest('./dist'));
});
/* eslint-enable no-use-before-define */